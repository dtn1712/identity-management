package com.rockitgroup.identity.management.interfaces.controller;

import com.rockitgroup.infrastructure.vitamin.common.controller.BaseController;
import com.rockitgroup.infrastructure.vitamin.common.dto.ResponseDTO;
import com.rockitgroup.infrastructure.vitamin.common.exception.BadRequestException;
import com.rockitgroup.infrastructure.vitamin.common.exception.NotFoundException;
import com.rockitgroup.infrastructure.vitamin.common.exception.UnauthorizedException;
import com.rockitgroup.identity.management.domain.model.account.Account;
import com.rockitgroup.identity.management.domain.model.account.AccountActivityTracking;
import com.rockitgroup.identity.management.domain.model.account.AccountToken;
import com.rockitgroup.identity.management.domain.model.account.AccountTokenActivityTracking;
import com.rockitgroup.identity.management.domain.model.enumeration.AccountActivityTypeEnum;
import com.rockitgroup.identity.management.domain.model.enumeration.AccountTokenActivityTypeEnum;
import com.rockitgroup.identity.management.domain.model.enumeration.ActivityResultEnum;
import com.rockitgroup.identity.management.domain.model.game.Game;
import com.rockitgroup.identity.management.domain.service.account.AccountActivityTrackingService;
import com.rockitgroup.identity.management.domain.service.account.AccountTokenActivityTrackingService;
import com.rockitgroup.identity.management.domain.service.account.AccountTokenService;
import com.rockitgroup.identity.management.domain.service.game.GameService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;
import java.util.Set;

@Slf4j
public abstract class AppBaseController extends BaseController {

    @Autowired
    private AccountTokenService accountTokenService;

    @Autowired
    private AccountActivityTrackingService accountActivityTrackingService;

    @Autowired
    private AccountTokenActivityTrackingService accountTokenActivityTrackingService;

    @Autowired
    private GameService gameService;


    public ResponseDTO generateFailResponse(HttpServletRequest servletRequest, ResponseDTO responseDTO,  AccountToken accountToken, AccountTokenActivityTypeEnum accountTokenActivityType, Throwable e) {
        logAccountTokenErrorActivity(servletRequest, accountTokenActivityType, accountToken, e.getMessage());
        return generateFailResponse(servletRequest, responseDTO, e);
    }

    public ResponseDTO generateFailResponse(HttpServletRequest servletRequest, ResponseDTO responseDTO, Account account, AccountActivityTypeEnum accountActivityType, Throwable e) {
        logAccountErrorActivity(servletRequest, accountActivityType, account, e.getMessage());
        return generateFailResponse(servletRequest, responseDTO, e);
    }

    public Game validateGame(Long gameId) {
        Game game = gameService.findActiveGameById(gameId);
        if (game == null) {
            throw new NotFoundException("Game does not exist or not active");
        }
        return game;
    }

    public AccountToken authenticateTokenRequest(HttpServletRequest request, Long gameId) {
        AccountToken accountToken = getAccountTokenFromRequest(request);
        if (accountToken == null) {
            throw new UnauthorizedException("Request is not authorized. Check header token");
        } else if (!isHaveGameAccount(accountToken.getAccount(), gameId)) {
            throw new BadRequestException("Invalid game account");
        }

        return accountToken;
    }

    public AccountToken authenticateTokenRequest(HttpServletRequest request) {
        AccountToken accountToken = getAccountTokenFromRequest(request);
        if (accountToken == null) {
            throw new UnauthorizedException("Request is not authorized. Check header token");
        }

        return accountToken;
    }

    public AccountToken getAccountTokenFromRequest(HttpServletRequest request) {
        AccountToken accountToken = null;
        String authorization = request.getHeader("Authorization");
        if (StringUtils.isNotEmpty(authorization)) {
            String[] authorizationData = authorization.split(":");
            if (authorizationData.length == 2) {
                accountToken = accountTokenService.findActiveToken(Long.parseLong(authorizationData[0]), authorizationData[1]);
            }
        }

        return accountToken;
    }

    public void logAccountTokenErrorActivity(HttpServletRequest servletRequest, AccountTokenActivityTypeEnum activityType, AccountToken accountToken, String info) {
        if (accountToken != null) {
            accountTokenActivityTrackingService.saveAsync(AccountTokenActivityTracking.create(accountToken,
                    activityType, servletRequest.getRemoteAddr(), ActivityResultEnum.FAIL, servletRequest.getRequestURI(), info));

        }
    }


    public void logAccountTokenSuccessActivity(HttpServletRequest servletRequest, AccountTokenActivityTypeEnum activityType, AccountToken accountToken, String info) {
        if (accountToken != null) {
            accountTokenActivityTrackingService.saveAsync(AccountTokenActivityTracking.create(accountToken,
                    activityType, servletRequest.getRemoteAddr(), ActivityResultEnum.SUCCESS, servletRequest.getRequestURI(), info));

        }
    }

    public void logAccountErrorActivity(HttpServletRequest servletRequest, AccountActivityTypeEnum activityType, Account account, String info) {
        if (account != null) {
            accountActivityTrackingService.saveAsync(AccountActivityTracking.create(account, activityType, servletRequest.getRemoteAddr(), ActivityResultEnum.FAIL, servletRequest.getRequestURI(), info));

        }
    }

    public void logAccountSuccessActivity(HttpServletRequest servletRequest, AccountActivityTypeEnum activityType, Account account, String info) {
        if (account != null) {
            accountActivityTrackingService.saveAsync(AccountActivityTracking.create(account, activityType, servletRequest.getRemoteAddr(), ActivityResultEnum.SUCCESS, servletRequest.getRequestURI(), info));

        }
    }

    public boolean isHaveGameAccount(Account account, Long gameId) {
        boolean isHaveGameAccount = false;
        Set<Game> games = account.getGames();
        for (Game game : games) {
            if (Objects.equals(game.getId(), gameId)) {
                isHaveGameAccount = true;
                break;
            }
        }
        return isHaveGameAccount;
    }

}
