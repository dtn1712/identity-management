package com.rockitgroup.identity.management.interfaces.configuration;

import com.rockitgroup.infrastructure.vitamin.common.configuration.BaseAuditorConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@Configuration
@EnableJpaAuditing
public class AuditorConfig extends BaseAuditorConfig {
}
