package com.rockitgroup.identity.management.interfaces.controller.game;

import com.rockitgroup.infrastructure.vitamin.common.dto.ResponseDTO;
import com.rockitgroup.identity.management.domain.dto.game.GameDTO;
import com.rockitgroup.identity.management.domain.model.game.Game;
import com.rockitgroup.identity.management.domain.service.game.GameService;
import com.rockitgroup.identity.management.interfaces.controller.AppBaseController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/api/v1/game")
public class GameController extends AppBaseController {

    @Autowired
    private GameService gameService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseDTO listAllGames(HttpServletRequest servletRequest) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            List<Game> games = gameService.getAllGames();
            return generateSuccessResponse(responseDTO, games, GameDTO.class);
        } catch (Exception e) {
            return generateFailResponse(servletRequest, responseDTO, e);
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseDTO findGame(HttpServletRequest servletRequest, @RequestParam(value = "key", required = true) String key) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            Game game = gameService.findActiveGameByKey(key);
            return generateSuccessResponse(responseDTO, game, GameDTO.class);
        } catch (Exception e) {
            return generateFailResponse(servletRequest, responseDTO, e);
        }
    }

}
