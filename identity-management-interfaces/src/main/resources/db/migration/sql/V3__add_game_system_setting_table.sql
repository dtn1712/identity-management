CREATE TABLE `game_system_settings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `gameId` bigint(20) NOT NULL,
  `systemType` varchar(50) NOT NULL,
  `settingKey` varchar(100) NOT NULL,
  `settingValue` varchar(100) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;