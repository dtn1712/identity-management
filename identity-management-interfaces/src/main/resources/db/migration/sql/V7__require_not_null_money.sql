ALTER TABLE `accounts`
  MODIFY COLUMN `money` int(11) NOT NULL DEFAULT 0 COMMENT 'Currency that can buy from real money';