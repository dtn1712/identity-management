ALTER TABLE `account_access_issues`
  ADD COLUMN `expiredAt` DATETIME DEFAULT NULL,
  MODIFY COLUMN `confirmedAt` datetime DEFAULT NULL,
  MODIFY COLUMN `sentAt` datetime DEFAULT NULL,
  MODIFY COLUMN `confirmIpAddress` varchar(100) DEFAULT NULL,
  ADD COLUMN `additionalInfo` varchar(1000) DEFAULT NULL;