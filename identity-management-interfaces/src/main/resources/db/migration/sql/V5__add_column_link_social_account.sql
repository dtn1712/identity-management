ALTER TABLE `account_social_links`
	DROP COLUMN `linkKey`,
	ADD COLUMN `gameId` bigint(20) NOT NULL,
	ADD COLUMN `socialAccountId` VARCHAR(100) NOT NULL,
	ADD COLUMN `socialAccountToken` VARCHAR(300) NOT NULL;