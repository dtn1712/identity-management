ALTER TABLE `accounts`
  ADD COLUMN `money` int(11) DEFAULT 0 COMMENT 'Currency that can buy from real money';

ALTER TABLE `account_access_issues`
  ADD COLUMN `gameId` bigint(20) NOT NULL;