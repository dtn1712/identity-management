-- DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(300) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `status` varchar(100) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- DROP TABLE IF EXISTS `account_devices`;
CREATE TABLE `account_devices` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `accountId` bigint(20) NOT NULL,
  `deviceUniqueId` varchar(300) NOT NULL,
  `deviceSecretKey` varchar(300) NOT NULL,
  `registeredAt` datetime NOT NULL,
  `registeredIpAddress` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- DROP TABLE IF EXISTS `account_access_issues`;
CREATE TABLE `account_access_issues` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `accountId` bigint(20) NOT NULL,
  `status` varchar(100) NOT NULL,
  `accessIssueType` varchar(100) NOT NULL,
  `confirmCode` varchar(300) NOT NULL,
  `confirmType` varchar(100) NOT NULL COMMENT 'Email or Phone',
  `confirmContact` varchar(300) NOT NULL,
  `confirmIpAddress` varchar(100) NOT NULL,
  `confirmedAt` datetime NOT NULL,
  `sentAt` datetime NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- DROP TABLE IF EXISTS `account_social_links`;
CREATE TABLE `account_social_links` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `accountId` bigint(20) NOT NULL,
  `socialLinkTypeId` bigint(20) NOT NULL,
  `linkKey` varchar(300) NOT NULL,
  `status` varchar(100) NOT NULL,
  `linkedAt` datetime NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- DROP TABLE IF EXISTS `social_link_types`;
CREATE TABLE `social_link_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `typeKey` varchar(100) NOT NULL COMMENT 'Facebook, Gmail, Game Center, etc',
  `url` varchar(100) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- DROP TABLE IF EXISTS `games`;
CREATE TABLE `games` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `key` varchar(100) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `gameType` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- DROP TABLE IF EXISTS `account_games`;
CREATE TABLE `account_games` (
  `accountsId` bigint(20) NOT NULL,
  `gamesId` bigint(20) NOT NULL,
  PRIMARY KEY (`accountsId`, `gamesId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- DROP TABLE IF EXISTS `account_game_settings`;
CREATE TABLE `account_game_settings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `accountId` bigint(20) NOT NULL,
  `gameId` bigint(20) NOT NULL,
  `settingKey` varchar(100) NOT NULL,
  `settingValue` varchar(100) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- DROP TABLE IF EXISTS `account_tokens`;
CREATE TABLE `account_tokens` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `accountId` bigint(20) NOT NULL,
  `token` varchar(300) NOT NULL,
  `status` varchar(100) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- DROP TABLE IF EXISTS `account_activity_trackings`;
CREATE TABLE `account_activity_trackings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `accountId` bigint(20) NOT NULL,
  `activityType` varchar(100) NOT NULL,
  `clientIpAddress` varchar(100) NOT NULL,
  `result` varchar(100) NOT NULL,
  `activityUrl` varchar(300) NOT NULL,
  `additionalInfo` varchar(1000) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- DROP TABLE IF EXISTS `account_token_activity_trackings`;
CREATE TABLE `account_token_activity_trackings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `accountTokenId` bigint(20) NOT NULL,
  `activityType` varchar(100) NOT NULL,
  `clientIpAddress` varchar(100) NOT NULL,
  `result` varchar(100) NOT NULL,
  `activityUrl` varchar(300) NOT NULL,
  `additionalInfo` varchar(1000) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `key` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- DROP TABLE IF EXISTS `account_groups`;
CREATE TABLE `account_groups` (
  `accountId` bigint(20) NOT NULL,
  `groupId` bigint(20) NOT NULL,
  PRIMARY KEY (`accountId`, `groupId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- DROP TABLE IF EXISTS `group_permissions`;
CREATE TABLE `group_permissions` (
  `groupId` bigint(20) NOT NULL,
  `permissions` varchar(255) NOT NULL,
  PRIMARY KEY (`groupId`,`permissions`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;