package com.rockitgroup.identity.management.domain.model.enumeration;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 11/4/18
 * Time: 3:08 PM
 */
public enum StatusEnum {

    ACTIVE,
    INACTIVE,
    PENDING,
    ERROR,
    CONFIRMED,
    EXPIRED
}
