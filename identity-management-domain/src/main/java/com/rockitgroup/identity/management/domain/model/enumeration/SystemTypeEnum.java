package com.rockitgroup.identity.management.domain.model.enumeration;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 11/11/18
 * Time: 12:07 AM
 */
public enum SystemTypeEnum {
    UPDATE_ACCOUNT_EMAIL,
    LOGIN_EMAIL
}
