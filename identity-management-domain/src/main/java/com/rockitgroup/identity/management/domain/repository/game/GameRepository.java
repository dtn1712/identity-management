package com.rockitgroup.identity.management.domain.repository.game;

import com.rockitgroup.identity.management.domain.model.enumeration.StatusEnum;
import com.rockitgroup.identity.management.domain.model.game.Game;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GameRepository extends BaseRepository<Game, Long>{

    Game findOneByIdAndStatus(Long id, StatusEnum status);

    Game findOneByKeyAndStatus(String key, StatusEnum status);

    @Query(value = "SELECT * FROM games g INNER JOIN account_games ag ON ag.gamesId=g.id WHERE g.status=:status AND ag.accountsId=:accountId", nativeQuery = true)
    List<Game> findAllGameByAccountId(@Param("accountId") Long accountId, @Param("status") String status);
}
