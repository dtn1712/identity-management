package com.rockitgroup.identity.management.domain.model.account;

import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import com.rockitgroup.identity.management.domain.model.enumeration.AccountTokenActivityTypeEnum;
import com.rockitgroup.identity.management.domain.model.enumeration.ActivityResultEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

@Entity
@Getter
@Setter
public class AccountTokenActivityTracking extends Modifiable {

    @OneToOne
    private AccountToken accountToken;

    @Enumerated(EnumType.STRING)
    private AccountTokenActivityTypeEnum activityType;

    @Enumerated(EnumType.STRING)
    private ActivityResultEnum result;

    private String clientIpAddress;
    private String activityUrl;
    private String additionalInfo;

    public static AccountTokenActivityTracking create(AccountToken accountToken, AccountTokenActivityTypeEnum activityType, String clientIpAddress, ActivityResultEnum result, String activityUrl, String additionalInfo) {
        AccountTokenActivityTracking accountTokenActivityTracking = new AccountTokenActivityTracking();
        accountTokenActivityTracking.setAccountToken(accountToken);
        accountTokenActivityTracking.setActivityType(activityType);
        accountTokenActivityTracking.setClientIpAddress(clientIpAddress);
        accountTokenActivityTracking.setResult(result);
        accountTokenActivityTracking.setActivityUrl(activityUrl);
        accountTokenActivityTracking.setAdditionalInfo(additionalInfo);
        return accountTokenActivityTracking;
    }
}
