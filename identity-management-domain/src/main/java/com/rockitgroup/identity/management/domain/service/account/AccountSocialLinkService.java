package com.rockitgroup.identity.management.domain.service.account;

import com.rockitgroup.identity.management.domain.repository.account.AccountSocialLinkRepository;
import com.rockitgroup.infrastructure.vitamin.common.service.DefaultBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class AccountSocialLinkService extends DefaultBaseService {

    @Autowired
    private AccountSocialLinkRepository accountSocialLinkRepository;

    @PostConstruct
    public void postConstruct() {
        this.repository = accountSocialLinkRepository;
    }

}
