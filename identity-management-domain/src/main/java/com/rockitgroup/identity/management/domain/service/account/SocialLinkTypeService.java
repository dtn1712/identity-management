package com.rockitgroup.identity.management.domain.service.account;

import com.rockitgroup.infrastructure.vitamin.common.service.DefaultBaseService;
import com.rockitgroup.identity.management.domain.model.account.SocialLinkType;
import com.rockitgroup.identity.management.domain.repository.account.SocialLinkTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class SocialLinkTypeService extends DefaultBaseService {

    @Autowired
    private SocialLinkTypeRepository socialLinkTypeRepository;

    @PostConstruct
    public void postConstruct() {
        this.repository = socialLinkTypeRepository;
    }

    public List<SocialLinkType> listAll() {
        return socialLinkTypeRepository.findAll();
    }

    public SocialLinkType findOneByTypeKey(String typeKey) {
        return socialLinkTypeRepository.findOneByTypeKey(typeKey);
    }

}
