package com.rockitgroup.identity.management.domain.service.account;

import com.rockitgroup.infrastructure.vitamin.common.service.DefaultBaseService;
import com.rockitgroup.identity.management.domain.model.account.AccountDevice;
import com.rockitgroup.identity.management.domain.repository.account.AccountDeviceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class AccountDeviceService extends DefaultBaseService {

    @Autowired
    private AccountDeviceRepository accountDeviceRepository;

    @PostConstruct
    public void postConstruct() {
        this.repository = accountDeviceRepository;
    }

    public AccountDevice findOneByDeviceUniqueIdAndDeviceSecretKey(String deviceUniqueId, String deviceSecretKey) {
        return accountDeviceRepository.findOneByDeviceUniqueIdAndDeviceSecretKey(deviceUniqueId, deviceSecretKey);
    }

}
