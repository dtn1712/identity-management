package com.rockitgroup.identity.management.domain.service.account;

import com.rockitgroup.infrastructure.vitamin.common.service.DefaultBaseService;
import com.rockitgroup.identity.management.domain.model.account.AccountToken;
import com.rockitgroup.identity.management.domain.model.enumeration.StatusEnum;
import com.rockitgroup.identity.management.domain.repository.account.AccountTokenRepository;
import com.rockitgroup.identity.management.domain.service.cache.AccountTokenCacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class AccountTokenService extends DefaultBaseService {

    @Autowired
    private AccountTokenRepository accountTokenRepository;

    @Autowired
    private AccountTokenCacheService accountTokenCacheService;

    @PostConstruct
    public void postConstruct() {
        this.repository = accountTokenRepository;
    }

    public AccountToken findActiveToken(Long accountId, String token) {
        return accountTokenRepository.findOneByAccountIdAndTokenAndStatus(accountId, token, StatusEnum.ACTIVE);
    }

    public void invalidateAllAccountTokens(Long accountId) {
        List<AccountToken> accountTokens = accountTokenRepository.findAllByAccountId(accountId);
        for (AccountToken accountToken : accountTokens) {
            accountToken.setStatus(StatusEnum.INACTIVE);
        }
        save(accountTokens);
    }


    @Override
    protected void clearCache(Object o) {
        AccountToken accountToken = (AccountToken) o;
        accountTokenCacheService.clearActiveTokenCache(accountToken.getId(), accountToken.getToken());
    }

    @Override
    protected void clearCaches(List list) {
        for (Object o : list) {
            AccountToken accountToken = (AccountToken) o;
            accountTokenCacheService.clearActiveTokenCache(accountToken.getId(), accountToken.getToken());
        }
    }

}
