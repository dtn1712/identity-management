package com.rockitgroup.identity.management.domain.service.account;

import com.rockitgroup.identity.management.domain.repository.account.AccountActivityTrackingRepository;
import com.rockitgroup.infrastructure.vitamin.common.service.DefaultBaseService;
import com.rockitgroup.identity.management.domain.model.account.AccountActivityTracking;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class AccountActivityTrackingService extends DefaultBaseService {

    @Autowired
    private AccountActivityTrackingRepository accountActivityTrackingRepository;

    @PostConstruct
    public void postConstruct() {
        this.repository = accountActivityTrackingRepository;
    }

    @Async
    public void saveAsync(AccountActivityTracking accountActivityTracking) {
        accountActivityTrackingRepository.saveAndFlush(accountActivityTracking);
    }
}
