package com.rockitgroup.identity.management.domain.model.game;

import com.rockitgroup.identity.management.domain.model.enumeration.SystemTypeEnum;
import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

@Entity
@Getter
@Setter
public class GameSystemSetting  extends Modifiable {

    @OneToOne
    private Game game;

    @Enumerated(EnumType.STRING)
    private SystemTypeEnum systemType;

    private String settingKey;
    private String settingValue;
}
