package com.rockitgroup.identity.management.domain.repository.account;

import com.rockitgroup.identity.management.domain.model.account.AccountToken;
import com.rockitgroup.identity.management.domain.model.enumeration.StatusEnum;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountTokenRepository extends BaseRepository<AccountToken, Long> {

    List<AccountToken> findAllByAccountId(Long accountId);

    AccountToken findOneByAccountIdAndStatusOrderByCreatedAtDesc(Long accountId, StatusEnum status);

    AccountToken findOneByAccountIdAndTokenAndStatus(Long accountId, String token, StatusEnum status);
}
