package com.rockitgroup.identity.management.domain.service.account;

import com.rockitgroup.infrastructure.vitamin.common.service.DefaultBaseService;
import com.rockitgroup.identity.management.domain.repository.account.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class GroupService extends DefaultBaseService {

    @Autowired
    private GroupRepository groupRepository;

    @PostConstruct
    public void postConstruct() {
        this.repository = groupRepository;
    }

}
