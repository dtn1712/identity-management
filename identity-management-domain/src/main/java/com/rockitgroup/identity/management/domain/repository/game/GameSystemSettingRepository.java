package com.rockitgroup.identity.management.domain.repository.game;

import com.rockitgroup.identity.management.domain.model.enumeration.SystemTypeEnum;
import com.rockitgroup.identity.management.domain.model.game.GameSystemSetting;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GameSystemSettingRepository extends BaseRepository<GameSystemSetting, Long> {

    List<GameSystemSetting> findAllByGameIdAndSystemType(Long gameId, SystemTypeEnum systemType);

    GameSystemSetting findOneByGameIdAndSystemTypeAndSettingKey(Long gameId, SystemTypeEnum systemType, String settingKey);
}
