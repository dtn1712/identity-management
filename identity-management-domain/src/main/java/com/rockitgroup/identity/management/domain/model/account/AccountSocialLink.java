package com.rockitgroup.identity.management.domain.model.account;

import com.rockitgroup.identity.management.domain.model.game.Game;
import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import com.rockitgroup.identity.management.domain.model.enumeration.StatusEnum;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

@Entity
@Getter
@Setter
public class AccountSocialLink extends Modifiable{

    @OneToOne
    private Account account;

    @OneToOne
    private SocialLinkType socialLinkType;

    @OneToOne
    private Game game;

    @Enumerated(EnumType.STRING)
    private StatusEnum status;

    private String socialAccountId;
    private String socialAccountToken;

    private DateTime linkedAt;

}
