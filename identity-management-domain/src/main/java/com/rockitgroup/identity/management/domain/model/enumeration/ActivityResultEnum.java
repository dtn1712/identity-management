package com.rockitgroup.identity.management.domain.model.enumeration;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 11/5/18
 * Time: 2:30 AM
 */
public enum ActivityResultEnum {
    SUCCESS, FAIL
}
