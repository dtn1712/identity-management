package com.rockitgroup.identity.management.domain.model.account;

import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import com.rockitgroup.identity.management.domain.model.enumeration.GroupPermissionEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Group extends Modifiable {

    private String name;
    private boolean active;
    private String key;

    @Enumerated(EnumType.STRING)
    @CollectionTable
    @Column
    @ElementCollection(targetClass = GroupPermissionEnum.class)
    private Set<GroupPermissionEnum> permissions;
}
