package com.rockitgroup.identity.management.domain.service.game;

import com.rockitgroup.infrastructure.vitamin.common.service.DefaultBaseService;
import com.rockitgroup.identity.management.domain.model.enumeration.SystemTypeEnum;
import com.rockitgroup.identity.management.domain.model.game.GameSystemSetting;
import com.rockitgroup.identity.management.domain.repository.game.GameSystemSettingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GameSystemSettingService extends DefaultBaseService {

    @Autowired
    private GameSystemSettingRepository gameSystemSettingRepository;

    @PostConstruct
    public void postConstruct() {
        this.repository = gameSystemSettingRepository;
    }

    public Map<String, String> findAllSettingsBySystemType(Long gameId, SystemTypeEnum systemType) {
        List<GameSystemSetting> gameSystemSettings = gameSystemSettingRepository.findAllByGameIdAndSystemType(gameId, systemType);
        Map<String, String> result = new HashMap<>();

        for (GameSystemSetting gameSystemSetting : gameSystemSettings) {
            result.put(gameSystemSetting.getSettingKey(), gameSystemSetting.getSettingValue());
        }

        return result;
    }

    public GameSystemSetting findSettingValue(Long gameId, SystemTypeEnum systemType, String key) {
        return gameSystemSettingRepository.findOneByGameIdAndSystemTypeAndSettingKey(gameId, systemType, key);
    }
}
