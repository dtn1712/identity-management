package com.rockitgroup.identity.management.domain.dto.mapper;

import com.rockitgroup.infrastructure.vitamin.common.dto.mapper.DTOMapper;
import com.rockitgroup.infrastructure.vitamin.common.dto.mapper.MapperFacadeFactory;
import com.rockitgroup.identity.management.domain.dto.account.AccountTokenDTO;
import com.rockitgroup.identity.management.domain.model.account.AccountToken;
import org.springframework.stereotype.Component;

@Component
public class AccountTokenDTOMappingHandler implements DTOMapper.MappingHandler<AccountToken, AccountTokenDTO> {

    @Override
    public AccountTokenDTO map(AccountToken model, Class<AccountTokenDTO> clazz) {
        AccountTokenDTO accountTokenDTO = MapperFacadeFactory.create().map(model, clazz);
        accountTokenDTO.setAccountId(model.getAccount().getId());
        accountTokenDTO.setToken(model.getToken());
        accountTokenDTO.setStatus(model.getStatus().name());
        return accountTokenDTO;
    }
}
