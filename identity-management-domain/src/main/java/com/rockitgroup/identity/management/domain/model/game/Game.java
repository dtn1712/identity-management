package com.rockitgroup.identity.management.domain.model.game;

import com.rockitgroup.identity.management.domain.model.enumeration.GameTypeEnum;
import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import com.rockitgroup.identity.management.domain.model.account.Account;
import com.rockitgroup.identity.management.domain.model.enumeration.StatusEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;


@Entity
@Getter
@Setter
public class Game extends Modifiable{

    private String name;
    private String key;
    private String description;

    @Enumerated(EnumType.STRING)
    private GameTypeEnum gameType;

    @Enumerated(EnumType.STRING)
    private StatusEnum status;

    @ManyToMany(mappedBy = "games", fetch = FetchType.LAZY)
    private Set<Account> accounts;

    public void setGameId(Long id) {
        this.setId(id);
    }
}
