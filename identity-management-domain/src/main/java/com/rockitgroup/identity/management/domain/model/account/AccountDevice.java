package com.rockitgroup.identity.management.domain.model.account;

import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import com.rockitgroup.identity.management.domain.model.enumeration.StatusEnum;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

@Entity
@Getter
@Setter
public class AccountDevice extends Modifiable{

    @OneToOne
    private Account account;

    private String deviceUniqueId;
    private String deviceSecretKey;

    private String deviceOSVersion;
    private String deviceModel;

    private DateTime registeredAt;
    private String registeredIpAddress;

    @Enumerated(EnumType.STRING)
    private StatusEnum status;

    public static AccountDevice create(Account account, String deviceUniqueId, String deviceSecretKey, String deviceOSVersion, String deviceModel, String registeredIpAddress) {
        AccountDevice accountDevice = new AccountDevice();
        accountDevice.setAccount(account);
        accountDevice.setDeviceUniqueId(deviceUniqueId);
        accountDevice.setDeviceSecretKey(deviceSecretKey);
        accountDevice.setDeviceOSVersion(deviceOSVersion);
        accountDevice.setDeviceModel(deviceModel);
        accountDevice.setRegisteredIpAddress(registeredIpAddress);
        accountDevice.setRegisteredAt(DateTime.now());
        accountDevice.setStatus(StatusEnum.ACTIVE);
        return accountDevice;
    }
}
