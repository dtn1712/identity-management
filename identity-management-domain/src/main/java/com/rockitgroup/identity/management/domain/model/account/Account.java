package com.rockitgroup.identity.management.domain.model.account;

import com.rockitgroup.identity.management.domain.model.game.AccountGameSetting;
import com.rockitgroup.identity.management.domain.model.game.Game;
import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import com.rockitgroup.identity.management.domain.model.enumeration.StatusEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
public class Account extends Modifiable {

    private String email;
    private String phone;

    private Integer money;

    @Enumerated(EnumType.STRING)
    private StatusEnum status;

    @ManyToMany(fetch = FetchType.LAZY)
    private Set<Game> games;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "account", fetch = FetchType.LAZY)
    private List<AccountGameSetting> gameSettings;

    public static Account create(Set<Game> games) {
        Account account = new Account();
        account.setStatus(StatusEnum.ACTIVE);
        account.setGames(games);
        return account;
    }

    public void setInfo(String email, String phone) {
        if (email != null) {
            this.email = email;
        }

        if (phone != null) {
            this.phone = phone;
        }
    }

}
