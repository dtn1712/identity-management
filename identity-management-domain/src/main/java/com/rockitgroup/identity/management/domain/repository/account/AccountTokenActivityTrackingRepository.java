package com.rockitgroup.identity.management.domain.repository.account;

import com.rockitgroup.identity.management.domain.model.account.AccountTokenActivityTracking;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountTokenActivityTrackingRepository extends BaseRepository<AccountTokenActivityTracking, Long> {
}
