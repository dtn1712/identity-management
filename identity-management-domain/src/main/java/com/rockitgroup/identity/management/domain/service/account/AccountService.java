package com.rockitgroup.identity.management.domain.service.account;

import com.rockitgroup.infrastructure.vitamin.common.service.DefaultBaseService;
import com.rockitgroup.identity.management.domain.model.account.Account;
import com.rockitgroup.identity.management.domain.model.account.AccountAccessIssue;
import com.rockitgroup.identity.management.domain.model.enumeration.AccessIssueConfirmTypeEnum;
import com.rockitgroup.identity.management.domain.model.enumeration.AccessIssueTypeEnum;
import com.rockitgroup.identity.management.domain.model.enumeration.StatusEnum;
import com.rockitgroup.identity.management.domain.model.enumeration.SystemTypeEnum;
import com.rockitgroup.identity.management.domain.model.game.Game;
import com.rockitgroup.identity.management.domain.repository.account.AccountRepository;
import com.rockitgroup.identity.management.domain.service.cache.AccountCacheService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.UnsupportedEncodingException;
import java.util.List;

@Slf4j
@Service
public class AccountService extends DefaultBaseService {

    private static final String UPDATE_ACCOUNT_EMAIL_CONFIRMATION_EMAIL_TEMPLATE = "update-account-email-confirmation-template.ftl";
    private static final String LOGIN_EMAIL_CONFIRMATION_EMAIL_TEMPLATE = "login-email-confirmation-template.ftl";

    @Autowired
    private AccountAccessIssueService accountAccessIssueService;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AccountCacheService accountCacheService;

    @PostConstruct
    public void postConstruct() {
        this.repository = accountRepository;
    }

    public Account findActiveAccountByEmail(String email) {
        if (email == null) {
            return null;
        }
        return accountRepository.findOneByStatusAndEmail(StatusEnum.ACTIVE, email);
    }

    public AccountAccessIssue confirmAccountAccessIssueEmailType(Account account, Game game, AccessIssueTypeEnum issueType, String confirmCode, String confirmIpAddress) {
        AccountAccessIssue accountAccessIssue = accountAccessIssueService.findActiveIssueByConfirmCode(account.getId(), game.getId(), issueType, AccessIssueConfirmTypeEnum.EMAIL, confirmCode);
        if (accountAccessIssue != null) {
            accountAccessIssueService.confirmIssue(accountAccessIssue, confirmIpAddress);
        }

        return accountAccessIssue;
    }

    public void startUpdateAccountEmailProcess(Account account, Game game, String email) throws Exception {
        accountAccessIssueService.createAccountAccessIssueEmailType(account, game, email, AccessIssueTypeEnum.UPDATE_ACCOUNT_EMAIL, SystemTypeEnum.UPDATE_ACCOUNT_EMAIL, UPDATE_ACCOUNT_EMAIL_CONFIRMATION_EMAIL_TEMPLATE);
    }

    public void startLoginEmailProcess(Account account, Game game, String email) throws UnsupportedEncodingException {
        accountAccessIssueService.createAccountAccessIssueEmailType(account, game, email, AccessIssueTypeEnum.LOGIN_EMAIL, SystemTypeEnum.LOGIN_EMAIL, LOGIN_EMAIL_CONFIRMATION_EMAIL_TEMPLATE);
    }

    @Override
    protected void clearCache(Object o) {
        Account account = (Account) o;
        accountCacheService.clearAccountCache(account.getId());
    }

    @Override
    protected void clearCaches(List list) {
        for (Object o : list) {
            Account account = (Account) o;
            accountCacheService.clearAccountCache(account.getId());
        }
    }
}
