package com.rockitgroup.identity.management.domain.repository.account;

import com.rockitgroup.identity.management.domain.model.account.AccountAccessIssue;
import com.rockitgroup.identity.management.domain.model.enumeration.AccessIssueTypeEnum;
import com.rockitgroup.identity.management.domain.model.enumeration.StatusEnum;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountAccessIssueRepository extends BaseRepository<AccountAccessIssue, Long> {

    @Query(value = "SELECT * FROM account_access_issues " +
            "WHERE accountId=:accountId AND gameId=:gameId " +
            "AND status=:status AND accessIssueType=:accessIssueType " +
            "AND confirmCode=:confirmCode AND confirmType=:confirmType LIMIT 1", nativeQuery = true)
    AccountAccessIssue findIssueByConfirmCode(@Param("accountId") Long accountId, @Param("gameId") Long gameId, @Param("status") String status,
                                              @Param("accessIssueType") String accessIssueType, @Param("confirmType") String confirmType,
                                              @Param("confirmCode") String confirmCode);

    List<AccountAccessIssue> findAllByAccountIdAndGameIdAndAccessIssueTypeAndStatus(Long accountId, Long gameId, AccessIssueTypeEnum accessIssueType, StatusEnum status);
}
