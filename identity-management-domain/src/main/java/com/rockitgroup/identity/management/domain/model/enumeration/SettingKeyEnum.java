package com.rockitgroup.identity.management.domain.model.enumeration;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 11/11/18
 * Time: 2:45 AM
 */
public enum SettingKeyEnum {

    EMAIL_SUBJECT("emailSubject");

    private String value;

    SettingKeyEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
