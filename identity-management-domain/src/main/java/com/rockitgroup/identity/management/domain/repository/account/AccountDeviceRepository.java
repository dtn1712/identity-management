package com.rockitgroup.identity.management.domain.repository.account;

import com.rockitgroup.identity.management.domain.model.account.AccountDevice;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountDeviceRepository extends BaseRepository<AccountDevice, Long> {

    AccountDevice findOneByDeviceUniqueIdAndDeviceSecretKey(String deviceUniqueId, String deviceSecretKey);

}
