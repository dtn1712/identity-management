package com.rockitgroup.identity.management.domain.model.enumeration;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 11/4/18
 * Time: 3:09 PM
 */
public enum AccessIssueConfirmTypeEnum {
    PHONE, EMAIL
}
